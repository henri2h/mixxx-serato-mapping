var MixarsPrimo = {};

MixarsPrimo.leds = {
  common: {},
  deck1: {
    jog: [0xb0, 0x0a],
    vueMeter: [0xb0, 0x1f]
  },
  deck2: {
    jog: [0xb1, 0x0a],
    vueMeter: [0xb1, 0x1f]
  },
  deck3: {
    jog: [0xb2, 0x0a],
    vueMeter: [0xb2, 0x1f]
  },
  deck4: {
    jog: [0xb3, 0x0a],
    vueMeter: [0xb3, 0x1f]
  },
};

MixarsPrimo.setLED = function (led, status) {
  if (status) {
    status = 0x7f;
  } else {
    status = 0x00;
  }
  midi.sendShortMsg(led[0], led[1], status);
};

MixarsPrimo.setGroupLED = function (group, selection, status) {
  if (status) {
    status = 0x7f;
  } else {
    status = 0x00;
  }
  var sel;
  if (group === "[Channel1]") sel = MixarsPrimo.leds.deck1;
  else if (group === "[Channel2]") sel = MixarsPrimo.leds.deck2;
  else if (group === "[Channel3]") sel = MixarsPrimo.leds.deck3;
  else if (group === "[Channel4]") sel = MixarsPrimo.leds.deck4;
  else return;

  var led = sel[selection];

  midi.sendShortMsg(led[0], led[1], status);
};

var decks = [
  {
    name: "deck1",
    playing: false,
    rotation: 0,
    endOfTrack: false
  },
  {
    name: "deck2",
    playing: false,
    rotation: 0,
    endOfTrack: false
  },
  {
    name: "deck3",
    playing: false,
    rotation: 0,
    endOfTrack: false
  },
  {
    name: "deck4",
    playing: false,
    rotation: 0,
    endOfTrack: false
  },
];

var getDeck = function (group) {
  var sel = 0;
  if (group === "[Channel1]") sel = 0;
  else if (group === "[Channel2]") sel = 1;
  else if (group === "[Channel3]") sel = 2;
  else if (group === "[Channel4]") sel = 3;
  else return;
  return decks[sel];
};

var addRotation = function (deck, value) {
  deck.rotation += value;
  if (deck.rotation > 0x18) deck.rotation = 1;
  if (deck.rotation <= 0) deck.rotation = 1;
};

var setRotation = function (deck, value, coef) {
  deck.rotation = value * coef * 10 % 0x17 + 1;
  if (deck.rotation > 0x18) deck.rotation = 1;
  if (deck.rotation <= 0) deck.rotation = 1;
};

var setVueMeter = function (deck, value) {
  deck.vueMeter = value * 0xa;

  // saturation
  if (deck.vueMeter > 0xa) deck.vueMeter = 0xa;
  if (deck.vueMeter <= 0) deck.vueMeter = 1;

  var led = MixarsPrimo.leds[deck.name].vueMeter;
  midi.sendShortMsg(led[0], led[1], deck.vueMeter);
};
/*
Mappings (LED) play:
0x90 : Deck1
0x91 : Deck2
0x92 : Deck3
Ox93 : Deck4

0x94 : 0x03 : Hot cue
0x94 : 0x07 : Roll
0x94 : 0x09 : Slicer
0x94 : 0x0B : Sampler

0x94 : 0x28 : Parameter 1
0x94 : 0x29 : Parameter 2

0x94 : 0x32 : Auto loop



Mappings (LED) : sampler + auto loop
0x94 : Deck 1
0x95 : Deck 2
0x96 : Deck 3
0x97 : Deck 4

Mappings : Effects 
0x98 : Deck 1 & 3
0x99 : Deck 2 & 4


0x9F : load button
0xB0 : 0x0A : jog wheel 1 0x45 : half
0xB1 : 0x0A : jog wheel 2
0x00 -> 0x
 -> 0x58 : full circle


 0x01 -> 0x19 : rotating circle

251 : some strange thinqs happens

0xB0 : 0x0C : 0x3 : jog weel center progress meter red
0xB0 : 0x0D : 0x3 : jog weel center progress meter green
0xB0 : 0x0E : 0x3 : jog weel center progress meter blue

0xB0 : 0x0D + 0x0E : magenta
0xB0 : 0x0C + 0x0D + 0x0D : white

0xB0 : 0x1F : Vue meter (volume progress bar)

// 0x90 -> 26 : headphones

*/

// The SysEx message to send to the controller to force the midi controller
// to send the status of every item on the control surface.
var ControllerStatusSysex = [0xf0, 0x00, 0x20, 0x7f, 0x03, 0x01, 0xf7];

var rotation = 0;
var progress = 0;
var center = 0;
var option = 0;

var toggleTimer = 0;
var toggleTimerEnabled = false;

MixarsPrimo.init = function () {
  // After midi controller receive this Outbound Message request SysEx Message,
  // midi controller will send the status of every item on the
  // control surface. (Mixxx will be initialized with current values)
  midi.sendSysexMsg(ControllerStatusSysex, ControllerStatusSysex.length);

  MixarsPrimo.decks = new components.ComponentContainer();
  MixarsPrimo.decks[1] = new MixarsPrimo.Deck(1, 0);
  MixarsPrimo.decks[2] = new MixarsPrimo.Deck(2, 1);
  MixarsPrimo.decks[3] = new MixarsPrimo.Deck(3, 2);
  MixarsPrimo.decks[4] = new MixarsPrimo.Deck(4, 3);


  // turn on all LEDs
  var pos = 0x1c;
  var mode = 0x1;

  // serpentint
  for (var l = 0x0; l < 0x7f; l++) midi.sendShortMsg(0x99, l, 0x0);
  // tests
  midi.sendShortMsg(0x90, 0x12, 0x7f);

  var pad = false;
  if (pad)
    engine.beginTimer(60, function () {
      for (var k = 0x1c; k <= 0x2c; k++) {
        midi.sendShortMsg(0x94, k, 0x0);
        midi.sendShortMsg(0x95, k, 0x0);
      }

      midi.sendShortMsg(0x94, pos, mode);
      midi.sendShortMsg(0x95, pos, mode);
      pos++;
      if (pos > 0x23) {
        pos = 0x1c;
        mode++;
        if (mode > 0x1f) mode = 1;
      }
    });



  var loop = true;
  if (loop)
    engine.beginTimer(40, function () {
      // Repeat the following code for the numbers 1 through 40

      for (var d = 0; d < 4; d++) {
        var channel = "[Channel" + (d + 1) + "]"

        var playing = engine.getParameter(channel, "play");
        var position = engine.getParameter(channel, "playposition")
        var duration = engine.getParameter(channel, "duration")

        print("playing : " + (d + 1) + " " + playing + " : " + position)
        playing = true;
        var deck = decks[d];
        if (deck !== undefined) {
          if (playing) {
            midi.sendShortMsg(
              MixarsPrimo.leds[deck.name].jog[0],
              0x0a,
              deck.rotation
            );
            setRotation(deck, position, duration);
          }


          if (deck.endOfTrack && toggleTimerEnabled) {
            midi.sendShortMsg(
              MixarsPrimo.leds[deck.name].jog[0],
              0x0c,
              0x5
            );
          }
          else {
            midi.sendShortMsg(
              MixarsPrimo.leds[deck.name].jog[0],
              0xc,
              0x0 // maximum
            );
            midi.sendShortMsg(
              MixarsPrimo.leds[deck.name].jog[0],
              0xd,
              0x0 // maximum
            );

            midi.sendShortMsg(
              MixarsPrimo.leds[deck.name].jog[0],
              0xe,
              0x0 // maximum
            );
          }
        }
      }

      toggleTimer++;
      var a = 15;
      toggleTimerEnabled = false;
      if (toggleTimer > a) toggleTimerEnabled = true;
      if (toggleTimer > 2 * a) toggleTimer = 0;

      var centerEnabled = false;
      if (centerEnabled) {
        if (option == 0 || option == 3 || option == 5 || option == 6) {
          midi.sendShortMsg(0xb0, 0x0c, center);
          midi.sendShortMsg(0xb1, 0x0c, center);
        }
        if (option == 1 || option == 3 || option == 4 || option == 6) {
          midi.sendShortMsg(0xb0, 0x0d, center);
          midi.sendShortMsg(0xb1, 0x0d, center);
        }
        if (option == 2 || option == 4 || option == 5 || option == 6) {
          midi.sendShortMsg(0xb0, 0x0e, center);
          midi.sendShortMsg(0xb1, 0x0e, center);
        }
        center++;
        if (center > 0xa) {
          center = 0;
          option++;
          if (option > 6) option = 0;
        }
      }
    });
};

MixarsPrimo.LED_COLORS = {
  off: 0x0,
  red: 0x1,
  blue: 0xB,
  blueAzure: 0xC, // looks like a bit white... can't say what it is
  greenPale: 0xD,
  green: 0xE,
  white: 0xF,
  blueVioled: 0x10,

  red_low: 0x11
}

// implement a constructor for a custom Deck object specific to your controller
MixarsPrimo.Deck = function (deckNumbers, midiChannel) {
  print("update deck")
  print(midiChannel)
  print(deckNumbers)



  // Call the generic Deck constructor to setup the currentDeck and deckNumbers properties,
  // using Function.prototype.call to assign the custom Deck being constructed
  // to 'this' in the context of the generic components.Deck constructor
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call
  components.Deck.call(this, deckNumbers);
  this.playButton = new components.PlayButton([0x90 + midiChannel, 0x00]);
  this.cueButton = new components.CueButton([0x90 + midiChannel, 0x01]);
  this.syncButton = new components.SyncButton([0x90 + midiChannel, 0x03]);

  this.pflButton = new components.Button({
    midi: [0x90 + midiChannel, 27],
    key: 'pfl',
  });

  this.hotcueButtons = [];
  this.selection = "sampler"

  this.hotcueButtons1 = [];
  this.hotcueButtons2 = [];

  this.update = function () {
    midi.sendShortMsg(0x94 + midiChannel, 0x3, 0);
    midi.sendShortMsg(0x94 + midiChannel, 0x7, 0x0);
    midi.sendShortMsg(0x94 + midiChannel, 0x9, 0x0);
    midi.sendShortMsg(0x94 + midiChannel, 0xB, 0x0);

    if (this.selection === "hotcue") midi.sendShortMsg(0x94 + midiChannel, 0x3, 0x7F);
    else if (this.selection === "roll") midi.sendShortMsg(0x94 + midiChannel, 0x07, 0x7F);
    else if (this.selection === "slicer") midi.sendShortMsg(0x94 + midiChannel, 0x09, 0x7F);
    else if (this.selection === "sampler") midi.sendShortMsg(0x94 + midiChannel, 0x0B, 0x7F);

    //this.hotcueButtons = [];
    for (var i = 1; i <= 8; i++) {
      var group = undefined;
      if (this.hotcueButtons[i] !== undefined) {
        var component = this.hotcueButtons[i];
        group = component.group;
        component.disconnect()
      }

      if (this.selection === "hotcue") {
        this.hotcueButtons[i] = new components.HotcueButton({
          midi: [0x94 + midiChannel, 0x1b + i],
          number: i,
          off: 0x0,
          group: group,
          colorMapper: new ColorMapper({
            0x32be44: 0x5, // green
            0xC50a08: 0x1 // red
          })
        });
      }
      else if (this.selection === "sampler") {
        for (var i = 1; i <= 8; i++) {
          this.hotcueButtons[i] = new components.SamplerButton({
            number: i,
            midi: [0x94 + midiChannel, 0x1b + i],
            
            empty: MixarsPrimo.LED_COLORS.red_low,
            on:0x10,
            playing: 0xE,
            looping: 0x5,
            loaded: MixarsPrimo.LED_COLORS.red,
            off: MixarsPrimo.LED_COLORS.off
          });
        }

      }

      else {
        this.hotcueButtons[i] = new components.HotcueButton({
          midi: [0x94 + midiChannel, 0x1b + i],
          number: i,
          off: 0x10,
          group: group, // dirty fix
          colorMapper: new ColorMapper({
            0x32be44: 0x12, // green
            0xC50a08: 0x1 // red
          })
        });

      }
    }

  }
  this.update();

  this.bt_hotcue = function (channel, control, value, status, group) {
    this.selection = "hotcue";
    this.update();
  };
  this.bt_roll = function (channel, control, value, status, group) {
    this.selection = "roll";
    this.update();
  };
  this.bt_slicer = function (channel, control, value, status, group) {
    this.selection = "slicer";
    this.update();
  };
  this.bt_sampler = function (channel, control, value, status, group) {
    this.selection = "sampler";
    this.update();
  };



  this.eqKnob = [];
  for (var k = 1; k <= 3; k++) {
    this.eqKnob[k] = new components.Pot({
      midi: [0xB0 + midiChannel, 0x02 + k],
      group: '[EqualizerRack1_' + this.currentDeck + '_Effect1]',
      inKey: 'parameter' + k,
    });
  }

  // ... define as many other Components as necessary ...

  // Set the group properties of the above Components and connect their output callback functions
  // Without this, the group property for each Component would have to be specified to its
  // constructor.
  this.reconnectComponents(function (c) {
    if (c.group === undefined) {
      // 'this' inside a function passed to reconnectComponents refers to the ComponentContainer
      // so 'this' refers to the custom Deck object being constructed
      c.group = this.currentDeck;
    }
  });
  // when called with JavaScript's 'new' keyword, a constructor function
  // implicitly returns 'this'
};


// give your custom Deck all the methods of the generic Deck in the Components library
MixarsPrimo.Deck.prototype = new components.Deck();



MixarsPrimo.btShift = function (channel, control, value, status, group) {
  print("val : ");

  var deck = MixarsPrimo.decks;
  if (value == 127) {
    deck.shift()
  } else {
    deck.unshift()
  }
};

var pos = 1;
MixarsPrimo.bt = function (channel, control, value, status, group) {
  print("val : " + value + " " + pos);

  midi.sendShortMsg(0x90, pos, 0x00);
  pos++;
  midi.sendShortMsg(0x90, pos, 0x7f);
};


// allow setting the deck selection, used for the loading track button
var deckSelected = [false, false];
MixarsPrimo.updateSelectedTrack = function (channel, control, value, status, group) {
  print("Update selected track " + group);
  if (group === "[Channel1]") deckSelected[0] = false
  else if (group === "[Channel2]") deckSelected[0] = true
  else if (group === "[Channel3]") deckSelected[1] = false
  else if (group === "[Channel4]") deckSelected[1] = true
};


MixarsPrimo.shutdown = function () {
  // turn off all LEDs
  for (var i = 1; i <= 40; i++) {
    midi.sendShortMsg(0x90, i, 0x00);
  }
};

// jog wheel
// The button that enables/disables scratching
MixarsPrimo.wheelTouch = function (channel, control, value, status, group) {
  var deckNumber = script.deckFromGroup(group);
  //if ((status & 0xF0) === 0x90) {    // If button down
  if (value === 0x7f) {
    // Some wheels send 0x90 on press and release, so you need to check the value
    var alpha = 1.0 / 8;
    var beta = alpha / 32;
    engine.scratchEnable(deckNumber, 128, 33 + 1 / 3, alpha, beta);
  } else {
    // If button up
    engine.scratchDisable(deckNumber);
  }
};

// The wheel that actually controls the scratching
MixarsPrimo.wheelTurn = function (channel, control, value, status, group) {
  // control that centers on 0x40 (64):
  var newValue = value - 64;

  addRotation(getDeck(group), newValue / 30);

  // --- End choice

  // In either case, register the movement
  var deckNumber = script.deckFromGroup(group);
  if (engine.isScratching(deckNumber)) {
    engine.scratchTick(deckNumber, newValue); // Scratch!
  } else {
    engine.setValue(group, "jog", newValue); // Pitch bend
  }
};

// callbacks
var makeConnection = function (name) {
  var callback = function (value, group, control) {
    MixarsPrimo.setGroupLED(group, name, value);
  };
  engine.makeConnection("[Channel1]", name, callback);
  engine.makeConnection("[Channel2]", name, callback);
};

var callback = function (value, group, control) {
  var sel = 0;
  if (group === "[Channel1]") sel = 0;
  else if (group === "[Channel2]") sel = 1;
  else return;

  decks[sel].playing = value;
  MixarsPrimo.setGroupLED(group, "play", value);
};
//engine.makeConnection("[Channel1]", "play_indicator", callback);
//engine.makeConnection("[Channel2]", "play_indicator", callback);

var callback = function (value, group, control) {
  var deck = getDeck(group);
  if (deck !== undefined)
    setVueMeter(deck, value);
};
engine.makeConnection("[Channel1]", "VuMeter", callback);
engine.makeConnection("[Channel2]", "VuMeter", callback);
engine.makeConnection("[Channel3]", "VuMeter", callback);
engine.makeConnection("[Channel4]", "VuMeter", callback);



var endOfTrackCallback = function (value, group, control) {
  print("end of track");
  var deck = getDeck(group);
  deck.endOfTrack = value;
};


var loadSelectedTrack = function (value, group, control) {
  print("loadingTrack");
  //MixarsPrimo.setGroupLED(group, "load", 0x1);
};

engine.makeConnection("[Channel1]", "end_of_track", endOfTrackCallback);
engine.makeConnection("[Channel2]", "end_of_track", endOfTrackCallback);
engine.makeConnection("[Channel3]", "end_of_track", endOfTrackCallback);
engine.makeConnection("[Channel4]", "end_of_track", endOfTrackCallback);


engine.makeConnection("[Channel1]", "LoadSelectedTrack", loadSelectedTrack);
engine.makeConnection("[Channel2]", "LoadSelectedTrack", loadSelectedTrack);
engine.makeConnection("[Channel3]", "LoadSelectedTrack", loadSelectedTrack);
engine.makeConnection("[Channel4]", "LoadSelectedTrack", loadSelectedTrack);